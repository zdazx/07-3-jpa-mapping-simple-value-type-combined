# TODO

## 要求 1

请在 main 模块中的 `com.twuc.webApp.domain.composite` 包中定义如下 SQL 所定义的两个 Entity：分别为 `CompanyProfile` 以及 `UserProfile`。对应如下两张数据表:

table: `company_profile`

| column | description    | additional                   |
|--------|----------------|------------------------------|
| id     | bigint         | auto\_increment, primary key |
| city   | varchar\(128\) | not null                     |
| street | varchar\(128\) | not null                     |

table: `user_profile`

| column          | description    | additional                   |
|-----------------|----------------|------------------------------|
| id              | bigint         | auto\_increment, primary key |
| address\_city   | varchar\(128\) | not null                     |
| address\_street | varchar\(128\) | not null                     |


并分别定义其 Repository：`CompanyProfileRepository` 和 `UserProfileRepository`。

## 要求 2

请在 test 模块中的 `com.twuc.webApp.domain.composite` 包中定义一个测试 `SimpleMappingAndValueTypeTest`。保存并读取一个 `CompanyProfile` 和 `UserProfile`。并观察生成的 SQL，是否满足第一问中的要求呢？