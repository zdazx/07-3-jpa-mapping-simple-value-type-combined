package com.twuc.webApp.domain.composite;

import com.twuc.webApp.domain.ClosureValue;
import com.twuc.webApp.domain.JpaTestBase;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SimpleMappingAndValueTypeTest extends JpaTestBase {

    @Autowired
    private CompanyProfileRepository companyProfileRepository;

    @Autowired
    private UserProfileRepository userProfileRepository;

    @Test
    void should_get_and_save_company_profile_value() {
        ClosureValue<Long> expectedId = new ClosureValue<>();
        flushAndClear(em -> {
            final CompanyProfile profile =
                    companyProfileRepository.save(new CompanyProfile("Xi'an", "star Road"));
            expectedId.setValue(profile.getId());
        });

        run(em -> {
            final CompanyProfile profile =
                    companyProfileRepository.findById(expectedId.getValue()).orElseThrow(
                            RuntimeException::new);

            assertEquals("Xi'an", profile.getCity());
            assertEquals("star Road", profile.getStreet());
        });
    }

    @Test
    void should_get_and_save_user_profile_value() {
        ClosureValue<Long> expectedId = new ClosureValue<>();
        flushAndClear(em -> {
            final UserProfile profile =
                    userProfileRepository.save(new UserProfile("Xi'an", "star Road"));
            expectedId.setValue(profile.getId());
        });

        run(em -> {
            final UserProfile profile =
                    userProfileRepository.findById(expectedId.getValue()).orElseThrow(
                            RuntimeException::new);

            assertEquals("Xi'an", profile.getAddressCity());
            assertEquals("star Road", profile.getAddressStreet());
        });
    }
}
